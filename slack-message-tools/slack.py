import argparse
import json
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import sys
import os
import keepachangelog

# print(os.environ)
# print(sys.executable)
CHANGELOG_FILE_NAME = "CHANGELOG.md"
RELEASE_NOTES_FILE_NAME = "release-notes.md"

firebase_block = [
    {
        "type": "context",
        "elements": [
            {
                "type": "image",
                "image_url": "https://www.gstatic.com/devrel-devsite/prod/v67cb8b0ddf4e3bae39406c7b2c88affae9b405bee7499568a4013f440189dfbf/firebase/images/favicon.png",
                "alt_text": "firebase"
            },
            {
                "type": "mrkdwn",
                "text": "Firebase Package `package_name` has been updated to version `package_version` by ci trigger"
            }
        ]
    }
]

sdk_block = [
    {
        "type": "context",
        "elements": [
            {
                "type": "mrkdwn",
                "text": " `sdk_package_name` has been updated to version `sdk_package_version`"
            }
        ]
    }
]

changelog_block = [
    {
        "type": "header",
        "text": {
            "type": "plain_text",
            "text": "Changelog - [sdk_changelog_type]",
            "emoji": True
        }
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "sdk_package_changelog"
        }
    }
]

release_notes_block = [
    {
        "type": "header",
        "text": {
                "type": "plain_text",
                "text": "Release Notes",
                "emoji": True
        }
    },
    {
        "type": "section",
        "text": {
            "type": "plain_text",
            "text": "sdk_package_release_notes",
            "emoji": True
        }
    }
]

def get_firebase_message(package_name, package_version):
    message = firebase_block
    message[0]["elements"][1]["text"] = message[0]["elements"][1]["text"].replace(
        "package_name", package_name)
    message[0]["elements"][1]["text"] = message[0]["elements"][1]["text"].replace(
        "package_version", package_version)
    message = json.dumps(message)
    print(message)
    return message

def get_sdk_message(sdk_package_name, sdk_package_version):
    message = sdk_block
    message[0]["elements"][0]["text"] = message[0]["elements"][0]["text"].replace(
        "sdk_package_name", sdk_package_name)
    message[0]["elements"][0]["text"] = message[0]["elements"][0]["text"].replace(
        "sdk_package_version", sdk_package_version)
    
    changelog = read_changelog(sdk_package_version)
    if changelog:
        changelog_block[0]["text"]["text"] = changelog_block[0]["text"]["text"].replace(
            "sdk_changelog_type", changelog[0])
        changelog_block[1]["text"]["text"] = changelog_block[1]["text"]["text"].replace(
            "sdk_package_changelog", changelog[1])
        
        message.append(changelog_block[0])
        message.append(changelog_block[1])

    release_notes = read_release_notes()
    if release_notes:
        release_notes_block[1]["text"]["text"] = release_notes_block[1]["text"]["text"].replace(
            "sdk_package_release_notes", release_notes)

        message.append(release_notes_block[0])
        message.append(release_notes_block[1])


    message = json.dumps(message)
    print(message)
    return message

def read_release_notes():
    if check_if_file_exists(RELEASE_NOTES_FILE_NAME):
        with open('release-notes.md', 'r') as file:
            data = file.read()

        return data
    else:
        return None

def check_if_file_exists(file_path):
    file_path = file_path.lower()
    if os.path.isfile(file_path):
        print(f"'{file_path}' exist.")
        return True
    else:
        print(f"'{file_path}' does not exist.")
        return False

def read_changelog(version):
    version_no = version
    if version_no.startswith('v'):
        version_no = version_no[1:]
        
    changelog_path = CHANGELOG_FILE_NAME
    
    if check_if_file_exists(changelog_path):
        changes = keepachangelog.to_dict(CHANGELOG_FILE_NAME)
        print(version)
        changes_in_version = changes[f"{version}"]
        return get_changelog_content(changes_in_version)
    else:
        return None
    
def get_changelog_content(changes_in_version):
    if 'fixed' in changes_in_version:
        fixed = changes_in_version['fixed']
        text = ('fixed', '\n'.join(fixed))
        print(text)
    elif 'added' in changes_in_version:
        added = changes_in_version['added']
        text = ('added', '\n'.join(added))
        print(text)
    elif 'changed' in changes_in_version:
        changed = changes_in_version['changed']
        text = ('changed', '\n'.join(changed))
        print(text)
    elif 'removed' in changes_in_version:
        removed = changes_in_version['removed']
        text = ('removed', '\n'.join(removed))
        print(text)
    elif 'deprecated' in changes_in_version:
        deprecated = changes_in_version['deprecated']
        text = ('deprecated', '\n'.join(deprecated))
        print(text)
    elif 'security' in changes_in_version:
        security = changes_in_version['security']
        text = ('security', '\n'.join(security))
        print(text)
    elif 'unreleased' in changes_in_version:
        unreleased = changes_in_version['unreleased']
        text = ('unreleased', '\n'.join(unreleased))
        print(text)
    else:
        text = None
    
    return text

# Initialize the Slack client
slack_token = os.environ.get('SLACK_TOKEN')
if not slack_token or not slack_token.strip():
    print("SLACK_TOKEN environment variable is not set or empty")
    sys.exit(1)
    
client = WebClient(token=slack_token)

# Define command-line arguments
parser = argparse.ArgumentParser(description='Send a message to Slack')
parser.add_argument('--channels', '-c', required=True, nargs='+', help='One or more Slack channels to send the message to')
parser.add_argument('--message', '-m', required=False,  help='The message to send to the Slack channels, in JSON format')
parser.add_argument('--text', '-t', required=False, help='The raw text message to send to the Slack channels')
parser.add_argument('--firebase', '-fb', nargs=2, help='Firebase package name and version /{package_name/}, /{version/}')
parser.add_argument('--sdk', '-sdk', nargs=2, help='SDK package name, version, changelog url and changelog /{package_name/}, /{version/}') 
parser.add_argument('--token', '-tk', required=False, help='Slack token')
parser.add_argument('--file', '-f', required=False, help='File path to upload file with message to Slack')

args = parser.parse_args()
for channel in args.channels: # Loop through the specified channels and send the message to each one
    try:
        if args.token:
            slack_token = args.token
            client = WebClient(token=slack_token)
        
        if args.file:
            response = client.files_upload(
                channels=channel,
                file=args.file
            )
            
        if args.text:
            response = client.chat_postMessage(
                channel=channel,
                text=args.text
            )
        elif args.message:
            message_dict = json.loads(args.message)
            response = client.chat_postMessage(
                channel=channel,
                blocks=message_dict["blocks"]
            )
        elif args.firebase:
            package_name, version = args.firebase
            firebase_block = get_firebase_message(package_name, version)
            response = client.chat_postMessage(
                channel=channel,
                blocks=firebase_block,
                text="Firebase Package Update"
            )
        elif args.sdk:
            package_name, version = args.sdk
            sdk_block = get_sdk_message(package_name, version)
            response = client.chat_postMessage(
                channel=channel,
                blocks=sdk_block,
                text="MG SGK Update"
            )
        
        if args.text:
            print(f"Message sent to {channel}: {args.text}")
        else:
            print(f"Message sent to {channel}")
    except SlackApiError as e:
        print(f"Error sending message to {channel}: {e}")
