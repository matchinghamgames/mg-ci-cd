## Script Documentation

The script sends a message to one or more Slack channels using the Slack API. The message can be in the form of a simple text message, a pre-formatted message defined as JSON, or a message containing information about package updates. The script takes command-line arguments to specify the Slack channels to send the message to and the type of message to send.

## Usage

```bash
python send_slack_message.py [-h] [--text TEXT] [--message MESSAGE] [--firebase PACKAGE_NAME VERSION]
                             [--sdk PACKAGE_NAME VERSION] channels [channels ...]
```


## Arguments

* `-c` or `channels`: A list of Slack channel IDs to send the message to.
* `t` or `--text`: The text message to send to the channels.
* `-m` or `--message`: A pre-formatted message in JSON format to send to the channels.
* `-fb` or `--firebase`: The name and version number of the Firebase package to send an update message about. The message will contain information about the new version of the package and a link to the release notes.
* `--sdk`: The name and version number of the MG SGK package to send an update message about. The message will contain information about the new version of the package and a link to the release notes.

## Code Explanation

The script first parses the command-line arguments using the `argparse` library. It then loops through the specified Slack channels and sends a message to each one.

If the `--text` argument is provided, the script sends a simple text message to the channels using the `chat_postMessage` method of the Slack API.

If the `--message` argument is provided, the script parses the JSON message using the `json` library, and sends the message to the channels using the `chat_postMessage` method of the Slack API.

If the `--firebase` argument is provided, the script gets the Firebase package name and version number from the arguments, and calls the `get_firebase_message` function to generate a message containing information about the new version of the package and a link to the release notes. The script then sends the message to the channels using the `chat_postMessage` method of the Slack API.

If the `--sdk` argument is provided, the script gets the MG SGK package name and version number from the arguments, and calls the `get_sdk_message` function to generate a message containing information about the new version of the package and a link to the release notes. The script then sends the message to the channels using the `chat_postMessage` method of the Slack API.

If an error occurs while sending a message to a channel, the script catches the error and prints an error message to the console.

After sending a message to all the specified channels, the script prints a success message to the console.
