# SDK CI Script Documentation

This is a GitLab CI script that is used to deploy and document web pages. Below is a detailed documentation of the script.

## Stages
- `deploy` - Deploys the web page to npm.
- `document` - Documents the web page.

## Variables
- `DOCS_PROJECT_PATH` - Path to the document web page.
- `SDK_PATH` - Path to the SDK.

## Anchors
- `.deploy-script` - Anchor that defines the deployment script.
- `.document-script` - Anchor that defines the documentation script.

## Jobs
### Base Jobs
- `.automated` - Base job that runs for tagged commits.
  - `image` - Docker image used for the job.
  - `only` - Runs only for tags.
  - `except` - Does not run for branches.
  - `allow_failure` - Allows the job to fail without affecting the overall pipeline status.
- `.publish` - Base job that deploys the web page to npm.
  - `image` - Docker image used for the job.

- `.document` - Base job that documents the web page.
  - `variables` - Defines the SDK path.
  - `tags` - Defines the runner for the job.
  - `only` - Runs only for the `main` branch.

### Action Jobs
- `automated.publish` - Job that automatically publishes the web page to npm.
  - `extends` - Inherits the `.automated` and `.publish` jobs.
  - `script` - Runs the deployment script defined in the `.deploy-script` anchor.

- `automated.document` - Job that automatically documents the web page.
  - `extends` - Inherits the `.document` job.
  - `only` - Runs only when there are changes to markdown files in the `main` branch.
  - `script` - Runs the documentation script defined in the `.document-script` anchor.

- `manual-document` - Job that manually documents the web page.
  - `extends` - Inherits the `.document` job.
  - `when` - Runs only when triggered manually.
  - `script` - Runs the documentation script defined in the `.document-script` anchor.

- `manual-publish` - Job that manually deploys the web page to npm.
  - `extends` - Inherits the `.publish` job.
  - `when` - Runs only when triggered manually.
  - `script` - Runs the deployment script defined in the `.deploy-script` anchor.

## Script Explanation
- The deployment script in the `.deploy-script` anchor removes any existing `.npmrc` file and creates a new one with the `NPM_REGISTRY` and `NPM_AUTH_TOKEN`. It then publishes the web page to npm using the `npm publish` command.

- The documentation script in the `.document-script` anchor checks if `changelog.md` and `README.md` files exist. If they do, it removes all files in the SDK path and copies `changelog.md` and `README.md` to the SDK path. It then copies the contents of the `img~` folder to the document web page's image folder. The script then changes directory to the document web page's path and executes `ssh-add`, `git fetch`, `git checkout`, `git pull`, `git add`, `git diff-index`, and `git commit` commands to update the SDK documents.

- The `automated.publish` job runs the deployment script automatically for tagged commits.

- The `automated.document` job runs the documentation script automatically for any changes to markdown files in the `main` branch.

- The `manual-document` job runs the documentation script manually and runs the change-documentation command when triggered manually

- The manual-publish job extends the .publish job and runs the deploy_to_npm command when triggered manually.
