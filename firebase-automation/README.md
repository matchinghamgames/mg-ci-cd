# Firebase Pipeline Configuration

This GitLab CI/CD pipeline script has three stages: downloading, deploy, and slack. It defines variables and job templates as anchors that are later used in the jobs.

## Variables

- `VERSION`: Firebase Package Version to be downloaded and deployed
- `SLACK_CHANNEL`: Slack Channel to send automation message

## Stages

### downloading

This stage has one job:
- `downloading-job`: It downloads and prepares the Firebase package by cloning the Git repository, fetching the latest changes from the `main` branch, downloading the package, and extracting it. This job is triggered by a push to the `main` branch.

### deploy

This stage has two jobs:
- `publish`: It deploys the Firebase package to the npm registry. This job is triggered by a tag push.
- `manual-publish`: It deploys the Firebase package to the npm registry manually.

### slack

This stage has two jobs:
- `slack-message`: It sends a Slack message with the details of the deployment. This job is triggered on a successful deployment.
- `m-slack-message`: It sends a Slack message with the details of the manual deployment. This job is triggered on a successful manual deployment.

## Anchors

- `.deploy-script`: Anchor for the script to deploy to the npm registry.
- `.downloading-script`: Anchor for the script to download and prepare the Firebase package.
- `.slack-script`: Anchor for the script to send a Slack message.

## Jobs

- `.default`: Base job with the `sdk-runner` tag.
- `downloading-job`: Job that downloads and prepares the Firebase package.
- `manual-downloading`: Job that downloads and prepares the Firebase package manually.
- `publish`: Job that deploys the Firebase package to the npm registry.
- `manual-publish`: Job that deploys the Firebase package to the npm registry manually.
- `slack-message`: Job that sends a Slack message with the details of the deployment.
- `m-slack-message`: Job that sends a Slack message with the details of the manual deployment.

Each job uses the appropriate anchor script, extends the `.default` job, and specifies its stage and when to run.

The `slack-message` and `m-slack-message` jobs depend on the `publish` and `manual-publish` jobs respectively and only run on a successful deployment. They also specify the `mg-3` tag to run on the matching runner.

Finally, there is a check to see if the job should be allowed to fail for the `downloading-job` job, and the `manual-publish` job is set to only run when manually triggered.
